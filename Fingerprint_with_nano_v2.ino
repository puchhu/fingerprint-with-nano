#include <FPM.h>                                                                            
#include<SoftwareSerial.h>
SoftwareSerial fserial(2, 3);
SoftwareSerial s(10,9);         ///////// RX , TX //////////
#include <Wire.h>

FPM finger(&fserial);
FPM_System_Params params;
char user_choice_enroll;
int fingerId;
int switch_press = 4;
int led_pin = 5;
int sensorValue;
int pre_switch_state;
int switch_state;
int sent_for_clearance = 0;
int data_for_final;
int back_data=0;
int execute_data=1;

void setup() {
  Serial.begin(9600);
  s.begin(9600);
  pinMode(switch_press,INPUT);
  pinMode(13,INPUT);
  pinMode(7,INPUT);
  pinMode(led_pin,OUTPUT);
  digitalWrite(led_pin,LOW);
  fserial.begin(57600);

  if (finger.begin()) {
    finger.readParams(&params);
    } else {
    Serial.println("Did not find fingerprint sensor :(");
    while (1) yield();
    }
}

void loop(){
  sensorValue = digitalRead(7);
  switch_state = digitalRead(switch_press);
 //Serial.println(switch_state);
  if((switch_state==HIGH)&&(pre_switch_state==LOW)){
    int16_t fid;
    if (get_free_id(&fid)){
      tone(8, 4000, 200);
      enroll_finger(fid);
    }
  }
  else{
    if(sensorValue==0){
      search_database();
    }
    pre_switch_state = switch_state;
  }
}  


///////////////////////////////////////////////////////////////  ENROLL ///////////////////////////////////////////////////////////////////////////////////////////////

bool get_free_id(int16_t * fid) {
  int16_t p = -1;
  for (int page = 0; page < (params.capacity / FPM_TEMPLATES_PER_PAGE) + 1; page++) {
    p = finger.getFreeIndex(page, fid);
    switch (p) {
      case FPM_OK:
        if (*fid != FPM_NOFREEINDEX) {
          return true;
        }
        break;
      case FPM_PACKETRECIEVEERR:
        return false;
      case FPM_TIMEOUT:
        return false;
      case FPM_READ_ERROR:
        return false;
      default:
        return false;
    }
    yield();
  }
  return false;
}

int16_t enroll_finger(int16_t fid) {
    int16_t p = -1;
     Serial.print(9001);
    while (p != FPM_OK) {
        p = finger.getImage();
        switch (p) {
            case FPM_OK:
                break;
            case FPM_NOFINGER:
                break;
            case FPM_PACKETRECIEVEERR:
                break;
            case FPM_IMAGEFAIL:
                break;
            case FPM_TIMEOUT:
                break;
            case FPM_READ_ERROR:
                break;
            default:
                break;
        }
        yield();
    }

    p = finger.image2Tz(1);
    switch (p) {
        case FPM_OK:
            break;
        case FPM_IMAGEMESS:
            return p;
        case FPM_PACKETRECIEVEERR:
            return p;
        case FPM_FEATUREFAIL:
            return p;
        case FPM_INVALIDIMAGE:
            return p;
        case FPM_TIMEOUT:
            return p;
        case FPM_READ_ERROR:
            return p;
        default:
            return p;
    }
    p = 0;
    while (p != FPM_NOFINGER) {
        p = finger.getImage();
        yield();
    }

    p = -1;
    Serial.print(9002);
    while (p != FPM_OK) {
        p = finger.getImage();
        switch (p) {
            case FPM_OK:
                break;
            case FPM_NOFINGER:
                break;
            case FPM_PACKETRECIEVEERR:;
                break;
            case FPM_IMAGEFAIL:
                break;
            case FPM_TIMEOUT:
                break;
            case FPM_READ_ERROR:
                break;
            default:
                break;
        }
        yield();
    }

    p = finger.image2Tz(2);
    switch (p) {
        case FPM_OK:
            break;
        case FPM_IMAGEMESS:
            return p;
        case FPM_PACKETRECIEVEERR:
            return p;
        case FPM_FEATUREFAIL:
            return p;
        case FPM_INVALIDIMAGE:
            return p;
        case FPM_TIMEOUT:
            return false;
        case FPM_READ_ERROR:
            return false;
        default:
            return p;
    }

    p = finger.createModel();
    if (p == FPM_OK) {
    } else if (p == FPM_PACKETRECIEVEERR) {
        return p;
    } else if (p == FPM_ENROLLMISMATCH) {
        return p;
    } else if (p == FPM_TIMEOUT) {
        return p;
    } else if (p == FPM_READ_ERROR) {
        return p;
    } else {
        return p;
    }

    p = finger.storeModel(fid);
    if (p == FPM_OK) {
      Serial.print(fid);
        return 0;
    } else if (p == FPM_PACKETRECIEVEERR) {
        return p;
    } else if (p == FPM_BADLOCATION) {
     
        return p;
    } else if (p == FPM_FLASHERR) {
        return p;
    } else if (p == FPM_TIMEOUT) {
        return p;
    } else if (p == FPM_READ_ERROR) {
        return p;
    } else {
        return p;
    }
}

/////////////////////////////////////////////////////////////// FINGERPRINT DETECTION ////////////////////////////////////////////////////////////

int search_database(void) {
    int16_t p = -1;
     while (p != FPM_OK) {
        p = finger.getImage();
        switch (p) {
            case FPM_OK:
                break;
            case FPM_NOFINGER:
                break;
            case FPM_PACKETRECIEVEERR:
                break;
            case FPM_IMAGEFAIL:
                break;
            case FPM_TIMEOUT:
                break;
            case FPM_READ_ERROR:
                break;
            default:
                break;
        }
        yield();
    }

    /* convert it */
    p = finger.image2Tz();
    switch (p) {
        case FPM_OK:
            break;
        case FPM_IMAGEMESS:
            return p;
        case FPM_PACKETRECIEVEERR:
            return p;
        case FPM_FEATUREFAIL:
            return p;
        case FPM_INVALIDIMAGE:
            return p;
        case FPM_TIMEOUT:
            return p;
        case FPM_READ_ERROR:
            return p;
        default:
            return p;
    }

    p = 0;
//    while (p != FPM_NOFINGER) {
//        p = finger.getImage();
//        yield();
//    }
    uint16_t fid, score;
    p = finger.searchDatabase(&fid, &score);
    if (p == FPM_OK) {
    } else if (p == FPM_PACKETRECIEVEERR) {
        return p;
    } else if (p == FPM_NOTFOUND) {
      Serial.print(9003);
        return p;
    } else if (p == FPM_TIMEOUT) {
        return p;
    } else if (p == FPM_READ_ERROR) {
        return p;
    } else {
        return p;
    }
    fingerId = fid;
    String fingerId1=String(fingerId);
    Serial.print(fingerId1);
    delay(1000);
}
